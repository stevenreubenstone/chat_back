require("dotenv").config();
import { RedisPubSub } from 'graphql-redis-subscriptions';
const accountSid = process.env.TWILIO1
const authToken = process.env.TWILIO2
const tclient = require('twilio')(accountSid, authToken);
const { gql, withFilter } = require('apollo-server');
const knexConfig = require("./db/knex").development;
const knex = require("knex")(knexConfig);
const { makeExecutableSchema } = require('graphql-tools');
const redis = require("redis");
const client = redis.createClient({ port: process.env.REDDISPORT, host: process.env.REDDISHOST, password: process.env.REDDISPW });
const pubsub = new RedisPubSub({
  publisher: client,
  subscriber: client
});
const CHAT_SENT_TOPIC = 'newChat';



const typeDefs = gql`

  type Chat {
    id: Int
    username: String
    message: String
  }

  type Query {
    getChats: [Chat]
  }

  type Mutation {
    createChat(username: String, message: String): Chat
  }

  type Subscription {
        chatSent: Chat    
  }

  
`;


const resolvers = {
  Query: {
    getChats: async () => {
      const chats = await knex.select().table('chats')

      return chats
    }
  },

  Mutation: {
    // createChat: async (parent: any, args: { username: any; message: any; }) => {
    createChat: async (parent: any, args: { username: any; message: any; }) => {
      const chat = await knex.insert({ username: args.username, message: args.message }).table('chats').returning('*')
      const data = {
        "__typename": "Chat",
        id: chat[0].id,
        username: chat[0].username,
        message: chat[0].message,
      }
      pubsub.publish(CHAT_SENT_TOPIC, { chatSent: data });

      if (chat[0].message.includes("idea") === true) {
        try {
          tclient.messages
            .create({
              body: `Idea Alert! User: ${chat[0].username} mentioned an idea in Idea Chat: "${chat[0].message}"`,
              from: '+15162179666',
              statusCallback: 'http://postb.in/1234abcd',
              to: `5164265510`
            })
            .then((message: { sid: any; }) => console.log(message.sid))
            .done();

        } catch (error) {
          console.log(error)
        }
      }
      return chat[0]
    },
  },

  Subscription: {
    chatSent: {
      subscribe: () => {
        console.log('here')
        return pubsub.asyncIterator(CHAT_SENT_TOPIC)
      },
    },
  },
}




const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

export { resolvers }

export default schema;


