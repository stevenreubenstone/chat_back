require("dotenv").config();
import express from 'express';
import schema from './apollo';
import graphqlHTTP from 'express-graphql';
import cors = require("cors");


const app = express();
const corsOptions = {
    credentials: true
};

app.use(cors(corsOptions));

app.use(
    "/graphql",
    graphqlHTTP(async req => {
        return {
            schema,
            context: req,
            graphiql: true
        }
    })
);

const server = app.listen(process.env.PORT, () => {
    console.log(`Listening on http server.`);
});


