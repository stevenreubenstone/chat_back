require("dotenv").config();
const knexConfig = require("./db/knex").development;
const knex = require("knex")(knexConfig);

module.exports = {
    countChats: (chatArray) => {
        if (chatArray.length === 0) { return 'There are no chats in here.' }
        const num = chatArray.length
        const text = `There are ${num} total chats in the Idea Chat`
        return text
    }
}