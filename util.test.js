const Helpers = require('./testFunctions.js');

test('test1 getChats', () => {
    expect(Helpers.countChats([1, 2])).toBe('There are 2 total chats in the Idea Chat');
});

test('test2 getChats', () => {
    expect(Helpers.countChats([1, 2, 3, 4, 5])).toBe('There are 5 total chats in the Idea Chat');
})

test('test3 getChats', () => {
    expect(Helpers.countChats([])).toBe('There are no chats in here.');
})



